﻿Imports System.IO

Public Class cLog

    Private ReadOnly Property AppPath() As String
        Get
            Return HttpRuntime.AppDomainAppPath & "\LogWeb_" & Date.Today.Year.ToString & Date.Today.Month.ToString & Date.Today.Day.ToString & ".txt"
        End Get
    End Property

    Public Shared Sub EscribeLog(ByVal Proc As String, ByVal Query As String, Optional ByVal Msj As String = "")
        Try
            Dim strArchivo = HttpRuntime.AppDomainAppPath & "\LogWeb_" & Date.Today.Year.ToString & Date.Today.Month.ToString & Date.Today.Day.ToString & ".txt"

            Dim SW As New StreamWriter(strArchivo, True)
            SW.WriteLine("/*" & Proc & " " & Date.Now.ToString("HH:mm:ss") & "*/ " & Query)
            SW.Close()
        Catch ex As Exception
        End Try
    End Sub

End Class
