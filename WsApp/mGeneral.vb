﻿Module mGeneral
    Public Function MyCulture() As System.Globalization.CultureInfo

        Dim CultureInfo As New System.Globalization.CultureInfo("es-AR")

        CultureInfo.NumberFormat.CurrencySymbol = "$"
        CultureInfo.NumberFormat.CurrencyDecimalSeparator = "."
        CultureInfo.NumberFormat.CurrencyGroupSeparator = ","
        CultureInfo.NumberFormat.CurrencyDecimalDigits = 2
        CultureInfo.NumberFormat.NumberDecimalSeparator = "."
        CultureInfo.NumberFormat.NumberGroupSeparator = ","
        CultureInfo.NumberFormat.NumberDecimalDigits = 2
        CultureInfo.NumberFormat.PercentDecimalSeparator = "."
        CultureInfo.NumberFormat.PercentGroupSeparator = ","
        CultureInfo.DateTimeFormat.LongTimePattern = "HH:mm:ss"
        CultureInfo.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
        CultureInfo.DateTimeFormat.LongDatePattern = "dd/MM/yyyy HH:mm:ss"

        Return CultureInfo

    End Function

    Public Function getPromo(ByVal sCodigo As String) As DataTable
        Dim sSql As String
        Dim dt As DataTable
        Dim lCon As DataAccess.IConexion

        Try

            lCon = DataAccess.DaoFactory.GetFactory()

            sSql = "exec sp_AsignarCupon '" & sCodigo & "'"

            lCon.Conectar()

            dt = lCon.getConsulta(sSql)

            lCon.Desconectar()

            Return dt
        Catch ex As Exception
            Return New DataTable
        End Try

    End Function


End Module
