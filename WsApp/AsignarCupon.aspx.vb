﻿Imports System.IO
Imports System.Net
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports PdfSharp
Imports PdfSharp.Drawing
Imports PdfSharp.Pdf

Public Class AsignarCupon
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        System.Threading.Thread.CurrentThread.CurrentCulture = MyCulture()
        Try

            If Not IsPostBack Then

                Dim sType As String = Request.QueryString("type")
                Dim sCode As String = Request.QueryString("code")
                Dim sToken As String = Request.QueryString("access_token")

                cLog.EscribeLog("Api/AsignarCupon", sType, "")
                cLog.EscribeLog("Api/AsignarCupon", sCode, "")

                If sType Is Nothing Then Exit Sub
                If sCode Is Nothing Then Exit Sub
                If sToken Is Nothing Then Exit Sub

                If Trim(sToken) <> Trim(ConfigurationManager.AppSettings.Get("Api_token")) Then

                    cLog.EscribeLog("Api/AsignarCupon", "Token Incorrecto: " & sToken, "")

                    Response.StatusCode = HttpStatusCode.Unauthorized
                    Response.StatusDescription = "Token no autorizado"
                    Response.Flush()
                    Response.Clear()
                    Response.ClearContent()
                    Response.Close()
                    Exit Sub

                End If

                Select Case sType
                    Case Is = "html"

                        If Not Request.HttpMethod = "GET" Then

                            cLog.EscribeLog("Api/AsignarCupon", "Metodo Incorrecto: " & sType, "")

                            Response.StatusCode = HttpStatusCode.BadRequest
                            Response.StatusDescription = "Metodo no valido"
                            Response.Flush()
                            Response.Clear()
                            Response.ClearContent()
                            Response.Close()
                            Exit Sub

                        End If

                        Dim dt As DataTable = getPromo(sCode)

                        If dt.Rows.Count = 0 Then

                            cLog.EscribeLog("Api/AsignarCupon", "No se encontraron cupones vigentes para la promo: " & sCode, "")

                            Response.StatusCode = HttpStatusCode.NotFound
                            Response.StatusDescription = "No existe el code en las promos"
                            Response.Flush()
                            Response.Clear()
                            Response.ClearContent()
                            Response.Close()
                            Exit Sub

                        End If

                        Dim sPathHtml As String = ConfigurationManager.AppSettings.Get("Html")
                        sPathHtml = Environment.ExpandEnvironmentVariables(sPathHtml)

                        Dim HtmlText = ""

                        If System.IO.File.Exists(sPathHtml) Then

                            HtmlText = My.Computer.FileSystem.ReadAllText(sPathHtml)

                        Else

                            cLog.EscribeLog("Api/AsignarCupon", "No existe el archivo: " & sPathHtml, "")
                            Response.StatusCode = HttpStatusCode.InternalServerError
                            Response.StatusDescription = "No existe el formato html"
                            Response.Flush()
                            Response.Clear()
                            Response.ClearContent()
                            Response.Close()
                            Exit Sub

                        End If

                        Dim sCodigoCupon As String = Trim(dt.Rows(0)("cup_codigo"))
                        Dim sImageCupon As String = ConfigurationManager.AppSettings.Get("Images") & "\" & sCode & ".jpg"

                        If Not System.IO.File.Exists(sImageCupon) Then

                            cLog.EscribeLog("Api/AsignarCupon", "No existe el archivo: " & sImageCupon)

                            Response.StatusCode = HttpStatusCode.InternalServerError
                            Response.StatusDescription = "Falta Imagen de la promocion"
                            Response.Flush()
                            Response.Clear()
                            Response.ClearContent()
                            Response.Close()
                            Exit Sub

                        End If

                        Dim sVencimientoCupon As String = dt.Rows(0)("cup_fechavto").ToString
                        Dim sQRCupon As String = ConfigurationManager.AppSettings.Get("api_QR") & "&" & ConfigurationManager.AppSettings.Get("api_QR_param1") & "=" & sCodigoCupon

                        sImageCupon = Replace(sImageCupon, "\", "/")

                        Dim htmlContent = String.Format(HttpUtility.HtmlDecode(HtmlText), ConfigurationManager.AppSettings.Get("Images_repo") & "/" & sCode & ".jpg", sCodigoCupon, sQRCupon, sVencimientoCupon)

                        'Dim ms As MemoryStream = New MemoryStream()
                        'Dim pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(htmlContent, PdfSharp.PageSize.A4, 0)
                        'pdf.Save(ms)

                        cLog.EscribeLog("Api/AsignarCupon", "CODIGO: " & sCodigoCupon & " QR: " & sQRCupon)

                        Dim bytes1 As Byte() = Encoding.UTF8.GetBytes(htmlContent)
                        Response.ContentType = "text/html"
                        Response.OutputStream.Write(bytes1, 0, bytes1.Length)
                        Response.StatusCode = HttpStatusCode.OK
                        Response.Headers.Add("ContentType", "text/html")
                        Response.Flush()
                        'Response.Clear()
                        'Response.ClearContent()
                        'Response.Close()
                        Exit Sub
                    Case Is = "pdf"

                        If Not Request.HttpMethod = "GET" Then

                            cLog.EscribeLog("Api/AsignarCupon", "Metodo Incorrecto: " & sType, "")

                            Response.StatusCode = HttpStatusCode.BadRequest
                            Response.StatusDescription = "Metodo no valido"
                            Response.Flush()
                            Response.Clear()
                            Response.ClearContent()
                            Response.Close()
                            Exit Sub

                        End If

                        Dim dt As DataTable = getPromo(sCode)

                        If dt.Rows.Count = 0 Then

                            cLog.EscribeLog("Api/AsignarCupon", "No se encontraron cupones vigentes para la promo: " & sCode, "")

                            Response.StatusCode = HttpStatusCode.NotFound
                            Response.StatusDescription = "No existe el code en las promos"
                            Response.Flush()
                            Response.Clear()
                            Response.ClearContent()
                            Response.Close()
                            Exit Sub

                        End If

                        Dim sPathHtml As String = ConfigurationManager.AppSettings.Get("pdf")
                        sPathHtml = Environment.ExpandEnvironmentVariables(sPathHtml)

                        Dim HtmlText = ""

                        If System.IO.File.Exists(sPathHtml) Then

                            HtmlText = My.Computer.FileSystem.ReadAllText(sPathHtml)

                        Else

                            cLog.EscribeLog("Api/AsignarCupon", "No existe el archivo: " & sPathHtml, "")
                            Response.StatusCode = HttpStatusCode.InternalServerError
                            Response.StatusDescription = "No existe el formato html"
                            Response.Flush()
                            Response.Clear()
                            Response.ClearContent()
                            Response.Close()
                            Exit Sub

                        End If

                        Dim sCodigoCupon As String = Trim(dt.Rows(0)("cup_codigo"))
                        Dim sImageCupon As String = ConfigurationManager.AppSettings.Get("Images") & "\" & sCode & ".jpg"

                        If Not System.IO.File.Exists(sImageCupon) Then

                            cLog.EscribeLog("Api/AsignarCupon", "No existe el archivo: " & sImageCupon)

                            Response.StatusCode = HttpStatusCode.InternalServerError
                            Response.StatusDescription = "Falta Imagen de la promocion"
                            Response.Flush()
                            Response.Clear()
                            Response.ClearContent()
                            Response.Close()
                            Exit Sub

                        End If

                        Dim sVencimientoCupon As String = dt.Rows(0)("cup_fechavto").ToString
                        Dim sQRCupon As String = ConfigurationManager.AppSettings.Get("api_QR") & "&" & ConfigurationManager.AppSettings.Get("api_QR_param1") & "=" & sCodigoCupon

                        sImageCupon = Replace(sImageCupon, "\", "/")

                        Dim htmlContent = String.Format(HttpUtility.HtmlDecode(HtmlText), sImageCupon, sCodigoCupon, sQRCupon, sVencimientoCupon)

                        Dim ms As MemoryStream = New MemoryStream()
                        Dim pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(htmlContent, PdfSharp.PageSize.A4, 0)
                        pdf.Save(ms)

                        cLog.EscribeLog("Api/AsignarCupon", "CODIGO: " & sCodigoCupon & " QR: " & sQRCupon)

                        Dim bytes1 As Byte() = ms.ToArray()
                        Response.ContentType = "application/pdf"
                        Response.OutputStream.Write(bytes1, 0, bytes1.Length)
                        Response.StatusCode = HttpStatusCode.OK
                        Response.Headers.Add("ContentType", "application/pdf")
                        Response.Flush()
                        'Response.Clear()
                        'Response.ClearContent()
                        'Response.Close()
                        Exit Sub
                    Case Else

                        cLog.EscribeLog("Api/AsignarCupon", "Type Incorrecto: " & sType, "")

                        Response.StatusCode = HttpStatusCode.BadRequest
                        Response.StatusDescription = "type incorrecto"
                        Response.Flush()
                        Response.Clear()
                        Response.ClearContent()
                        Response.Close()
                        Exit Sub

                End Select

            End If

        Catch ex As Exception
            cLog.EscribeLog("Api/AsignarCupon", ex.Message, "")
        End Try
    End Sub


End Class
