﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Help.aspx.vb" Inherits="WsAPP.Help" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Help APP API</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://getbootstrap.com/docs/4.0/assets/css/docs.min.css" />
    <link rel="stylesheet" href="https://getbootstrap.com/docs/4.0/assets/css/syntax.css" />
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <style>
        .hll {
            background-color: #ffc;
        }

        .c {
            color: #999;
        }

        .k {
            color: #069;
        }

        .o {
            color: #555;
        }

        .cm {
            color: #999;
        }

        .cp {
            color: #099;
        }

        .c1 {
            color: #999;
        }

        .cs {
            color: #999;
        }

        .gd {
            background-color: #fcc;
            border: 1px solid #c00;
        }

        .ge {
            font-style: italic;
        }

        .gr {
            color: #f00;
        }

        .gh {
            color: #030;
        }

        .gi {
            background-color: #cfc;
            border: 1px solid #0c0;
        }

        .go {
            color: #aaa;
        }

        .gp {
            color: #009;
        }

        .gu {
            color: #030;
        }

        .gt {
            color: #9c6;
        }

        .kc {
            color: #069;
        }

        .kd {
            color: #069;
        }

        .kn {
            color: #069;
        }

        .kp {
            color: #069;
        }

        .kr {
            color: #069;
        }

        .kt {
            color: #078;
        }

        .m {
            color: #f60;
        }

        .s {
            color: #d44950;
        }

        .na {
            color: #4f9fcf;
        }

        .nb {
            color: #366;
        }

        .nc {
            color: #0a8;
        }

        .no {
            color: #360;
        }

        .nd {
            color: #99f;
        }

        .ni {
            color: #999;
        }

        .ne {
            color: #c00;
        }

        .nf {
            color: #c0f;
        }

        .nl {
            color: #99f;
        }

        .nn {
            color: #0cf;
        }

        .nt {
            color: #2f6f9f;
        }

        .nv {
            color: #033;
        }

        .ow {
            color: #000;
        }

        .w {
            color: #bbb;
        }

        .mf {
            color: #f60;
        }

        .mh {
            color: #f60;
        }

        .mi {
            color: #f60;
        }

        .mo {
            color: #f60;
        }

        .sb {
            color: #c30;
        }

        .sc {
            color: #c30;
        }

        .sd {
            font-style: italic;
            color: #c30;
        }

        .s2 {
            color: #c30;
        }

        .se {
            color: #c30;
        }

        .sh {
            color: #c30;
        }

        .si {
            color: #a00;
        }

        .sx {
            color: #c30;
        }

        .sr {
            color: #3aa;
        }

        .s1 {
            color: #c30;
        }

        .ss {
            color: #fc3;
        }

        .bp {
            color: #366;
        }

        .vc {
            color: #033;
        }

        .vg {
            color: #033;
        }

        .vi {
            color: #033;
        }

        .il {
            color: #f60;
        }

        .css .o,
        .css .o + .nt,
        .css .nt + .nt {
            color: #999;
        }

        .language-bash::before,
        .language-sh::before {
            color: #009;
            content: "$ ";
            user-select: none;
        }

        .language-powershell::before {
            color: #009;
            content: "PM> ";
            user-select: none;
        }
    </style>

</head>

<body>

    <div id="body">

        <main class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content" role="main">

            <h1 class="bd-title" id="content">GET API/AsignarCupon</h1>

            <div>
                <h2 id="examples">Request Information<a class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#" style="padding-left: 0.375em;">
                        </a></h2>

                <hr/>

                <h3>URI Parameters</h3>

                <p>None.</p>

                <h3>Body Parameters</h3>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Type</th>
                            <th>Additional information</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>type</td>
                            <td>
                                <p>pdf</p>
                            </td>
                            <td>string
                            </td>
                            <td>
                                <p>Required</p>
                            </td>
                        </tr>
                        <tr>
                            <td>code</td>
                            <td>
                                <p>codigo de la promocion</p>
                            </td>
                            <td>integer
                            </td>
                            <td>
                                <p>Required</p>
                            </td>
                        </tr>
                        <tr>
                            <td>access_token</td>
                            <td>
                                <p>Acces Token</p>
                            </td>
                            <td>string
                            </td>
                            <td>
                                <p>Required</p>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <p></p>

                <h2>Response Information</h2>

                <p>application/pdf</p>

                <div>

                    <div>
                        <span><b>Sample:</b></span>
                        <p>GET /Api/AsignarCupon?type=pdf&code=700&access_token=access_token HTTP/1.1</p>
                    </div>
                </div>

            </div>

        </main>
    </div>
</body>
</html>
