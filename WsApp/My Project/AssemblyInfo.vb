﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie los valores de estos atributos para modificar la información
' asociada a un ensamblado.

' Revisar los valores de los atributos del ensamblado
<Assembly: AssemblyTitle("WsApp")> 
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("WsApp")> 
<Assembly: AssemblyCopyright("Copyright ©  2020")> 
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'El siguiente GUID es para el Id. typelib cuando este proyecto esté expuesto a COM
<Assembly: Guid("19a9961e-c1d5-4bdd-9d93-3b65b272ad1e")> 

' La información de versión de un ensamblado consta de los siguientes cuatro valores:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o aceptar los valores predeterminados de los números de compilación y de revisión 
' mediante el carácter '*', como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>
