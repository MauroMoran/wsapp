﻿Imports System.Web.Routing
Imports Microsoft.AspNet.FriendlyUrls

Public NotInheritable Class RouteConfig
    Private Sub New()
    End Sub
    Public Shared Sub RegisterRoutes(routes As RouteCollection)
        Dim settings = New FriendlyUrlSettings()
        settings.AutoRedirectMode = RedirectMode.Permanent
        routes.EnableFriendlyUrls(settings)
        routes.MapPageRoute("", "Api", "~/help.aspx")
        routes.MapPageRoute("", "Api/AsignarCupon", "~/AsignarCupon.aspx")
        routes.MapPageRoute("", "", "~/help.aspx")
    End Sub
End Class